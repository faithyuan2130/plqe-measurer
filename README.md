# plqe-measurer

Thanks to Grey Christoforo for basically entirely rewriting this code!!

This program measures the PLQE of your samples with one common background/empty measurement,  nonlinearity corrections to the spectrum measurements, and stray light correction in the analysis.